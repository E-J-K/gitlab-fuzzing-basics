# GitLab Fuzzing Basics

This tutorial contains lessons for learning how to leverage Fuzzing within GitLab.
It highlights the key benefits to using Coverage-Based Fuzzing as well as Web-API Fuzzing.

## Target Audience

The intended audience is:

- Developers who want to learn about Fuzzing
- Developers wishing to enhance application security

## Labs

- [Pre-Requisites](./docs/01-prerequisites.md)
- [Running the Application](./docs/02-running-the-application.md)
- [Coverage Fuzzing](./docs/03-coverage-based-fuzzing.md)
- [Web API Fuzzing](./docs/04-web-api-fuzzing.md)
