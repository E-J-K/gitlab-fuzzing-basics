# Web-API Based Fuzz-Testing

GitLab allows you to add web-api fuzz testing to your pipelines. This helps you discover bugs and potential security issues that other QA processes may miss. API fuzzing performs fuzz testing of API operation parameters. Fuzz testing sets operation parameters to unexpected values in an effort to cause unexpected behavior and errors in the API back-end.

In this section we'll learn how to configure Web-API based fuzzing. We will also go through examining the results. 

## Step 1: Configuration

1. Open the **WebIDE**  

![](../images/f1.png) 

2. Create a new file named `.gitlab-api-fuzzing.yml` in the project's root directory and add the contents of [gitlab-api-fuzzing-config.yml](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing/-/blob/master/gitlab-api-fuzzing-config.yml)

![](../images/w1.png) 

3. Create a new file named `test_openapi.v2.0.json` with the OpenAPI spec seen below in the project's root directory, making sure that **INGRESS_ENDPOINT** is replaced with your Ingress Endpoint obtained in [Deploying Applications with Pipeline](./02-deploying-application-with-pipeline.md) lesson
```
  {
  "swagger": "2.0",
  "info": {
    "version": "1.0",
    "title": "Adding Notes",
    "description": "Testing adding notes via simply simple notes"
  },
  "host": "INGRESS_ENDPOINT",
  "basePath": "/notes",
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/add": {
      "post": {
        "description": "User provides a note to add to the database",
        "summary": "POST api to create a note",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "message",
            "in": "body",
            "required": true,
            "description": "The user provided text used to create a note",
            "schema": {
              "$ref": "#/definitions/CreateNoteRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Note added successfully!"
          }
        }
      }
    }
  },
  "definitions": {
    "CreateNoteRequest": {
      "title": "Create NoteRequest",
      "example": {
        "message": "meow"
      },
      "type": "object",
      "properties": {
        "message": {
          "description": "The user provided text used to create a note",
          "example": "meow",
          "type": "string"
        }
      },
      "required": [
        "message"
      ]
    }
  }
}
```

4. Select the **.gitlab-ci.yml** file

![](../images/f2.png) 

5. Add the following to `.gitlab-ci.yml`
```
include:
  - template: API-Fuzzing.gitlab-ci.yml
```

6. Add the following to `.gitlab-ci.yml`, remember to replace
the **INGRESS_ENDPOINT** with your ingress endpoint
```
variables:
  FUZZAPI_PROFILE: Quick-10
  FUZZAPI_OPENAPI: test_openapi.v2.0.json
  FUZZAPI_TARGET_URL: http://INGRESS_ENDPOINT
```

7. Update the stages in `.gitlab-ci.yml`
```
stages:
  ...
  - fuzz
```

8. Select Commit:

![](../images/s3.png)

9. Select commit to master:

![](../images/s4.png)

9. By going back to the projects main page, you can see that a pipeline is running:

![](../images/w2.png)


## Step 2: Adding a vulnerability

Now let's add a vulnerability to see Web API fuzzing in action.

1. Open the WebIDE

![](../images/f1.png) 

2. Select **notes/routes.py**

![](../images/n1.png)

3. Change the following in the `add_note()` function in `notes/routes.py`
```
if len(msg) > 1024:
        return jsonify({"Error": "Message tooooo long!"}), 400
```

to

```
if len(msg) > 10:
        return jsonify({"Error": "Message tooooo long!"}), 500
```

4. Press the **Commit** button

![](../images/s3.png)

5. Verify the code

![](../images/n2.png)

6. Select **Create a new branch** and **Start a new merge request**, and press **Commit**

![](../images/n3.png)

7. Give the MR a title and a description

![](../images/n4.png)

8. Press the **Submit merge request** button

![](../images/n5.png)

7. Now you'll see the MR with a pipeline running

![](../images/n6.png)

## Step 3: Viewing the Results

1. When the pipeline completes, press **Expand** under the **Security Scan** section

![](../images/a1.png)

**Note**: You may need to refresh your browser

2. Click on the **Unknown Fuzzing injection via 'message' on 'POST'** vulnerability

![](../images/a2.png)

3. Here you have information on the Exception caught by fuzzing

![](../images/a3.png)
![](../images/a4.png)

Here you can see that an un-handled exception was picked up. This is because the server threw a 500 instead of handling the error. Ideally a database error should cause the 500, this is just a forced test.

---

Congratulations! You have now successfully completed this workshop. You can now checkout either the [DevSecOps](https://gitlab.com/tech-marketing/workshops/devsecops/gitlab-devsecops-basics) or [Active Security](https://gitlab.com/tech-marketing/workshops/devsecops/gitlab-protect-basics) Workshops.