# Coverage Based Fuzz-Testing

GitLab allows you to add coverage-guided fuzz testing to your pipelines. This helps you discover bugs and potential security issues that other QA processes may miss. Coverage-guided fuzzing sends random inputs to an instrumented version of your application in an effort to cause unexpected behavior, such as a crash. Such behavior indicates a bug that you should address.

In this section we'll learn how to configure coverage-based fuzzing. We will also go through examining the results.

## Step 1: Configuration

1. Open the **WebIDE**  

![](../images/f1.png)

2. Use the sidebar to open `.gitlab-ci.yml`

![](../images/f2.png)

3. Add the **include** section:
```
include:
  - template: Coverage-Fuzzing.gitlab-ci.yml
```

4. Add to the bottom of the file (take special note on spacing):
```
my_fuzz_target:
  image: python:3.9.1-buster
  extends: .fuzz_base
  stage: c-fuzz
  script:
    - pip install --extra-index-url https://gitlab.com/api/v4/projects/19904939/packages/pypi/simple pythonfuzz
    - ./gitlab-cov-fuzz run --engine pythonfuzz -- fuzz.py
```

5. Update the stages in `.gitlab-ci.yml`
```
stages:
    - build
    - c-fuzz
    - deploy
```

6. Add a new file named `fuzz.py` under the root directory

![](../images/f3.png)

7. Add the following contents to the file:
```
from html.parser import HTMLParser
from pythonfuzz.main import PythonFuzz

@PythonFuzz
def fuzz(buf):
    try:
        string = buf.decode("ascii")
        parser = HTMLParser()
        parser.feed(string)
    except UnicodeDecodeError:
        pass

if __name__ == '__main__':
    fuzz()
```

7. Select Commit:

![](../images/f4.png)

8. Select **Create a new branch**, give the branch a name, check **Start a new merge request** and press the **Commit** button

![](../images/f5.png)

9. Give the merge request a name and description

![](../images/f6.png)

10. Press the **Create merge request** button

![](../images/f7.png)

11. Wait for the pipeline to complete

![](../images/f8.png)

**NOTE**: The c-fuzz job will show failure, but this is to be expected, we can see the results once the job completes.

## Step 2: Viewing the Results in MR

1. Once the pipeline has complete, we can expand the **Security scanning** section

![](../images/m1.png)

**NOTE**: You may need to refresh the page

2. Select the **Uncaught-exception** Vulnerability

![](../images/m2.png)

3. Here we can see the vulnerability in full detail

![](../images/m3.png)

Here you can see that an un-handled exception was picked up. This is because we are just passing instead of performing a function.

---

Now you've learned about coverage-based fuzzing! We can now checkout [fuzzing on a running web API](./04-web-api-fuzzing.md).
